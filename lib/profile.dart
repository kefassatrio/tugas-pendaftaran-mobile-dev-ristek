import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'About Me',
          style: TextStyle(
            fontFamily: 'Montserrat',
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.grey[850],
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(child: Container(width: 200,child: Image(image: AssetImage("assets/kefas.png")))),
              Padding(padding: EdgeInsets.only(top: 10),child: Center(child: Text("Kefas Satrio", textAlign: TextAlign.center, style: TextStyle(color: Colors.grey[850], fontFamily: "Montserrat", fontSize: 35, fontWeight: FontWeight.bold),))),
              Center(
                child: Container(
                    width: 320,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text(
                          "My name is Kefas Satrio Bangkit Solideantyo, but you can call me Bang Ganteng",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontFamily: "Montserrat")
                      ),
                    )
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 50),child: Text("Here are my hobbies:", textAlign: TextAlign.left, style: TextStyle(color: Colors.grey[850], fontFamily: "Montserrat", fontSize: 25, fontWeight: FontWeight.bold),)),
              Padding(padding: EdgeInsets.only(top: 10),child: Text("- Listening to music\n- Watching online videos\n- Gaming\n- Badminton", textAlign: TextAlign.left, style: TextStyle(color: Colors.grey[850], fontFamily: "Montserrat", fontSize: 20),)),
              Padding(padding: EdgeInsets.only(top: 50),child: Text("Here are my social medias:", textAlign: TextAlign.left, style: TextStyle(color: Colors.grey[850], fontFamily: "Montserrat", fontSize: 25, fontWeight: FontWeight.bold),)),
              Padding(padding: EdgeInsets.only(top: 10),child: Text("- Instagram : kefas_satrio\n- Line: cephas_knight\n- Twitter: @kefas_satrio", textAlign: TextAlign.left, style: TextStyle(color: Colors.grey[850], fontFamily: "Montserrat", fontSize: 20),)),

            ],
          ),
        ),
      ),
    );
  }
}
