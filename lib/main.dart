import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'store/reducer.dart';
import './jumpscare.dart';
import './calculator.dart';
import './landingpage.dart';
import './profile.dart';

void main() {
  final Store<Map> store = Store<Map>(darkReducer, initialState: initialState);
  runApp(App(store));
}

class App extends StatelessWidget {
  final Store<Map> store;
  App(this.store);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        initialRoute: '/',
        routes: {
          '/': (context) => LandingPage(),
          '/calculator': (context) => Calculator(),
          '/profile': (context) => Profile(),
          '/jumpscare': (context) => JumpScare(),
        },
      )
    );

  }
}
