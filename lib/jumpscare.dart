import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class JumpScare extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Secret Calculator',
          style: TextStyle(
            fontFamily: 'Montserrat',
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.grey[850],
        elevation: 0.0,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 100),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text("Salah kode gan ;)", style: TextStyle(color: Colors.white, fontFamily: "Montserrat", fontSize: 20), textAlign: TextAlign.center,),
            Padding(
                padding: EdgeInsets.only(top: 20),
                child: FittedBox(child: Image(image: AssetImage("assets/jumpscare.gif")), fit: BoxFit.fill,)
            )
          ],
        ),
      ),
    );
  }
}