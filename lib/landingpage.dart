import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
//import 'package:redux/redux.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  String insertedCode;
  final passController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    passController.dispose();
    super.dispose();
  }
  Widget kasiTau(insertedCode){
    if((insertedCode != null && insertedCode != "") && (insertedCode != '2' && insertedCode != '20' && insertedCode != '202' && insertedCode != '2020')){
      print(insertedCode != '2');
      return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Text("Psst... pin nya bukan '$insertedCode' gan", style: TextStyle(fontFamily: "Montserrat", color: Colors.white),),
      );
    }
    return Container( width: 0.0, height: 0.0);
  }
  @override
  Widget build(BuildContext context) {
//    StoreConnector<bool,String>(
//        converter: (store) => store.state.toString(),
//        builder: (context, viewModel) {
//          if(viewModel == "true"){
//            return Text("Khintil");
//          }
//          return Text(viewModel,style: TextStyle(fontSize: 24));
//        }
//    ),
    return
      StoreConnector<Map,Map>(
          converter: (store) => {'globalState':store.state},
        builder: (context, viewModel) {
          return Scaffold(
            backgroundColor: viewModel["globalState"]["isDark"] ? Colors.grey[900] : Colors.white,
            appBar: AppBar(
              centerTitle: true,
              title: Text(
                "Secret Calculator",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Colors.white,
                ),
              ),
              backgroundColor: Colors.grey[850],
              elevation: 0.0,
              automaticallyImplyLeading: false,
            ),
            body: Padding(
                padding: EdgeInsets.fromLTRB(15.0, 70.0, 15.0, 0.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 200.0,
                              child: TextField(
                                onChanged: (text)
                                {this.setState(() {
                                  insertedCode = text;
                                });},
                                controller: passController,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: viewModel["globalState"]["isDark"] ? Colors.white : Colors.grey[900],
                                ),
                                cursorColor: Colors.white,
                                decoration: InputDecoration(
                                  fillColor: viewModel["globalState"]["isDark"] ? Colors.white : Colors.grey[850],
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: viewModel["globalState"]["isDark"] ? Colors.white : Colors.grey[850], width: 2.0),
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: viewModel["globalState"]["isDark"] ? Colors.white30 : Colors.grey[850], width: 1.0),
                                  ),
                                  border: OutlineInputBorder(),
                                  labelStyle: TextStyle(color: viewModel["globalState"]["isDark"] ? Colors.grey : Colors.grey[850]),
                                  labelText: 'Secret code',
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 15.0, 0, 0),
                              child: FlatButton(
                                  highlightColor: viewModel["globalState"]["isDark"] ? Colors.red : Colors.grey[850],
                                  color: viewModel["globalState"]["isDark"] ? Colors.redAccent[400] : Colors.grey[700],
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 5.0),
                                  onPressed: () {
                                    if (insertedCode == '2020') {
                                      Navigator.pushNamed(
                                          context, '/calculator');
                                    }
                                    else {
                                      Navigator.pushNamed(
                                          context, '/jumpscare');
                                    }
                                  },
                                  child: Text(
                                    "Submit",
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ),
                            ),
                            kasiTau(insertedCode)

                          ]
                      )
                    ]
                )
            ),
          );
        });
  }
}