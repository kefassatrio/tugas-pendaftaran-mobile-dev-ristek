import './actions.dart';

Map darkReducer(Map previousState, dynamic action) {
  switch(action){
    case ReducerActions.CHANGE_THEME:
      return {
        ...previousState, "isDark":!previousState["isDark"]
      };
  };
}

var initialState = {
  'isDark':false,
};