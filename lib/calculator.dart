import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kefas_ganteng/store/reducer.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:flutter_redux/flutter_redux.dart';
//import 'package:redux/redux.dart';
import './store/actions.dart';
import './main.dart';

class Calculator extends StatefulWidget {
  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {

  String equation = "0";
  String result = "0";
  String expression = "";
  double equationFontSize = 38.0;
  double resultFontSize = 48.0;
//  bool isDark = true;

  void btnClicked(String btnText){
    setState(() {
      if(btnText == "C"){
        equation = "0";
        result = "0";
        equationFontSize = 38.0;
        resultFontSize = 48.0;
      }
      else if(btnText == "⌫"){
        equationFontSize = 48.0;
        resultFontSize = 38.0;
        equation = equation.substring(0, equation.length - 1);
        if(equation == ""){
          equation = "0";
        }
      }
      else if(btnText == "="){
        equationFontSize = 38.0;
        resultFontSize = 48.0;

        expression = equation;
        expression = expression.replaceAll("x", "*");

        try{
          Parser p = new Parser();
          Expression exp = p.parse(expression);

          ContextModel cm = ContextModel();
          result = "${exp.evaluate(EvaluationType.REAL, cm)}";
          equation = result;
        }catch(e){
          result = "Error";
        }
      }
      else{
        equationFontSize = 48.0;
        resultFontSize = 38.0;
        if(equation == "0"){
          equation = btnText;
        }
        else{
          if(btnText == "+" || btnText == "-" || btnText == "x" || btnText == "/"){
            if(equation.substring(equation.length-1) != btnText){
              equation = equation + btnText;
            }
          }
          else{
            equation = equation + btnText;
          }
        }
      }
    });
  }

  Widget customButton(String btnVal, bool isDark){
    Color customColor;
    Color customTextColor;
    double customFontSize;
    if(btnVal == "=" ){
      customFontSize = 25;
      customColor = isDark ? Colors.red[700] : Colors.grey[850];
      customTextColor = Colors.white;
    }
    else{
      customFontSize = 25;
      customTextColor = Colors.grey[850];
    }
    return  Expanded(
      child: Container(
        color: customColor,
        child: OutlineButton(
          padding: EdgeInsets.all(25.0),
          onPressed: () => btnClicked(btnVal),
          child: Text(
            "$btnVal",
            style: TextStyle(
              fontFamily: "Montserrat",
              fontSize: customFontSize,
              color: isDark ? Colors.white : customTextColor,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<Map,Map>(
    converter: (store) => {'globalState':store.state},
    builder: (context, viewModel) {
      print(viewModel["globalState"]["isDark"]);
    return Scaffold(
        backgroundColor: Colors.white,
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.black87,
                ),
                child: Text(
                  'Secret Calculator',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.account_circle),
                title: Text('About me'),
                onTap: () {Navigator.pushNamed(context, '/profile');},
              ),
              ListTile(
                leading: Icon(Icons.arrow_back),
                title: Text('Logout'),
                onTap: () {Navigator.pushNamed(context, '/');},
              ),
            ],
          ),
        ),
        appBar: AppBar(
          centerTitle: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Secret Calculator',
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  color: Colors.white,
                ),
              ),
          StoreConnector<Map,VoidCallback>(
              converter: (store) {
                return () => store.dispatch(ReducerActions.CHANGE_THEME);
              },
              builder: (context, callback) {
                return Switch(
                  value: viewModel["globalState"]["isDark"],
                  onChanged: (val) => callback(),
                  activeTrackColor: Colors.red,
                  activeColor: Colors.white,
              );}
              )
            ],
          ),
          backgroundColor: Colors.grey[850],
          elevation: 0.0,
        ),
        body: Container(
            color: viewModel["globalState"]["isDark"] ? Colors.grey[850] : Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        alignment:  Alignment.bottomRight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.all(10.0),
                                alignment:  Alignment.bottomRight,
                                child: Text(
                                  "$equation",
                                  style: TextStyle(
                                    color: viewModel["globalState"]["isDark"] ? Colors.white : Colors.grey[850],
                                    fontSize: equationFontSize,
                                    fontFamily: "Montserrat",
                                    fontWeight: FontWeight.w600,
                                  ),

                                )
                            ),
                            Container(
                                padding: EdgeInsets.all(10.0),
                                alignment:  Alignment.bottomRight,
                                child: Text(
                                  "$result",
                                  style: TextStyle(
                                    color: viewModel["globalState"]["isDark"] ? Colors.white : Colors.grey[850],
                                    fontSize: resultFontSize,
                                    fontFamily: "Montserrat",
                                    fontWeight: FontWeight.w600,
                                  ),

                                )
                            ),
                          ],
                        ),
                      ),
                    )
                ),
                Container(
                    color: viewModel["globalState"]["isDark"] ? Colors.grey[850] : Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            customButton("7", viewModel["globalState"]["isDark"]),
                            customButton("8", viewModel["globalState"]["isDark"]),
                            customButton("9", viewModel["globalState"]["isDark"]),
                            customButton("+", viewModel["globalState"]["isDark"]),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            customButton("4", viewModel["globalState"]["isDark"]),
                            customButton("5", viewModel["globalState"]["isDark"]),
                            customButton("6", viewModel["globalState"]["isDark"]),
                            customButton("-", viewModel["globalState"]["isDark"]),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            customButton("1", viewModel["globalState"]["isDark"]),
                            customButton("2", viewModel["globalState"]["isDark"]),
                            customButton("3", viewModel["globalState"]["isDark"]),
                            customButton("x", viewModel["globalState"]["isDark"]),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            customButton("C", viewModel["globalState"]["isDark"]),
                            customButton("0", viewModel["globalState"]["isDark"]),
                            customButton("⌫", viewModel["globalState"]["isDark"]),
                            customButton("/", viewModel["globalState"]["isDark"]),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            customButton("=", viewModel["globalState"]["isDark"]),
                          ],
                        ),
                      ],
                    )
                ),
              ],
            )
        )
    );
    });
  }
}